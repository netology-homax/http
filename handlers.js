let stock = [];
let counter = 0;

exports.createItem = (req, res) => {
    res.writeHead(200, {'content-type': 'application/json'});
    let body = '';
    req.on('data', (chunk) => {
      body += chunk.toString();
    });
    req.on('end', () => {
        let decodedBody = JSON.parse(body);
        if (!decodedBody.name || !decodedBody.quantity) {
            res.StatusCode = 400;
            res.end('Bad params');
            return;
        }
        let item = {
            id: ++counter,
            name: decodedBody.name,
            quantity: decodedBody.quantity
        };
        stock.push(item);
        res.end(JSON.stringify(item));
    });
}

exports.addItem = (req, res) => {
    res.writeHead(200, {'content-type': 'application/json'});
    let body = '';
    req.on('data', (chunk) => {
      body += chunk.toString();
    });
    req.on('end', () => {
        let decodedBody = JSON.parse(body);
        if (!decodedBody.id || !decodedBody.quantity) {
            res.StatusCode = 400;
            res.end('Bad params');
            return;
        }
        let itemIndex = stock.findIndex(item => item.id === decodedBody.id);
        if (itemIndex === -1) {
            res.writeHead(404, {'content-type': 'text/plain'});
            res.end('item not found');
            return;
        }
        let item = stock[itemIndex];
        item.quantity += decodedBody.quantity
        res.end(JSON.stringify(item));
    });
}

exports.removeItem = (req, res) => {
    res.writeHead(200, {'content-type': 'application/json'});
    let body = '';
    req.on('data', (chunk) => {
      body += chunk.toString();
    });
    req.on('end', () => {
        let decodedBody = JSON.parse(body);
        if (!decodedBody.id || !decodedBody.quantity) {
            res.StatusCode = 400;
            res.end('Bad params');
            return;
        }
        let itemIndex = stock.findIndex(item => item.id === decodedBody.id);
        if (itemIndex === -1) {
            res.writeHead(404, {'content-type': 'text/plain'});
            res.end('item not found');
            return;
        }
        let item = stock[itemIndex];
        if (item.quantity < decodedBody.quantity) {
            res.writeHead(400, {'content-type': 'text/plain'});
            res.end('Not enough quantity');
            return;
        }
        item.quantity -= decodedBody.quantity;
        res.end(JSON.stringify(item));
    });
}

exports.getItems = (req, res) => {
    res.writeHead(200, {'content-type': 'application/json'});
    res.end(JSON.stringify(stock));
}
