const http = require('http');
const url = require('url');
const port = process.env.PORT || 3000;
const handlers = require('./handlers');

let routes = {
    "/create": "createItem",
    "/add": "addItem",
    "/remove": "removeItem",
    "/get": "getItems"
};

http.createServer((req, res) => {
    let path = url.parse(req.url).pathname;
    let route = findRoute(path);
    if (path) {
        handlers[route](req, res);
        return;
    }
    res.StatusCode = 404;
    res.end('Page not found');
}).listen(port)
    .on('listening', () => {
        console.log(`Start service on port ${port}...\n`);
        console.log(`
Commands:
    - GET /get - получение остатков на складе
    - POST /create - создание позиции
    - POST /add - добавлении позиции
    - POST /remove - удаление позиции
        `);
    });

function findRoute(path) {
    for (let route of Object.keys(routes)) {
        if (path === route) {
            return routes[route];
        }
    }
    return false;
}
